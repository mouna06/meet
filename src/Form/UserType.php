<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('password')
            ->add('pseudo')
            ->add('birthday')
            ->add('sexe')
            ->add('taille')
            ->add('haircolor')
            ->add('eyescolor')
            ->add('poids')
            ->add('musique')
            ->add('film')
            ->add('livre')
            ->add('activity')
            ->add('description')
            ->add('citation')
            ->add('othersexe')
            ->add('otheragemin')
            ->add('otheragemax')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
