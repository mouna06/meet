<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class InfoUserFirstFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pseudo')
            ->add('birthday', DateType::class, [
        'label' => 'Date de naissance',
    ])
            ->add('sexe',
                ChoiceType::class, [
                    'label' => 'Sexe',
                    'choices' => [
                        'Femme' => 1,
                        'Homme' => 2,
                        'Autre' => 3,
                    ],
                    'choice_attr' => [
                        'Femme' => ['data-color' => 'Red'],
                        'Homme' => ['data-color' => 'Yellow'],
                        'Autre' => ['data-color' => 'Green'],
                    ],
                ])
            ->add('taille', IntegerType::class, [
                'label' => 'Taille',
            ])
            ->add('haircolor', TextType::class, [
                'label' => 'Couleur de cheveux',
            ])
            ->add('eyescolor', TextType::class, [
                'label' => 'Yeux',
            ])
            ->add('poids', IntegerType::class, [
                'label' => 'Poids',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
